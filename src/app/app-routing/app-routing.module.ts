import { NotFoundComponent } from './../component/not-found/not-found.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from '../component/dashboard/dashboard.component';
import { ProductComponent } from '../component/product/product.component';
import { AddProductComponent } from '../component/add-product/add-product.component';
import { ProductInfoComponent } from '../component/product-info/product-info.component';

const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'products', component: ProductComponent },
  { path: 'product/:id', component: ProductInfoComponent },
  { path: 'addProduct', component: AddProductComponent },
  { path: '**', component: NotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: [],
})
export class AppRoutingModule {}
