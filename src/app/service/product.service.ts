import { Product } from './../model/product';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  private url: string = 'https://fakestoreapi.com/products';
  constructor(private httpClient: HttpClient) {}

  getProducts(): Promise<any> {
    return this.httpClient.get(this.url).toPromise();
  }

  getProduct(productId: number): Promise<any> {
    return this.httpClient.get(this.url + '/' + productId).toPromise();
  }

  createProduct(product: Product): Promise<any> {
    console.log('createProduct() run');
    return this.httpClient.post(this.url, product).toPromise();
  }
}
