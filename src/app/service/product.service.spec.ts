import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { AppRoutingModule } from '../app-routing/app-routing.module';
import { AppComponent } from '../app.component';
import { AddProductComponent } from '../component/add-product/add-product.component';
import { DashboardComponent } from '../component/dashboard/dashboard.component';
import { NavbarComponent } from '../component/navbar/navbar.component';
import { NotFoundComponent } from '../component/not-found/not-found.component';
import { ProductInfoComponent } from '../component/product-info/product-info.component';
import { ProductComponent } from '../component/product/product.component';

import { ProductService } from './product.service';

describe('ProductService', () => {
  let service: ProductService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        ProductComponent,
        AddProductComponent,
        DashboardComponent,
        NavbarComponent,
        NotFoundComponent,
        ProductInfoComponent,
      ],
      imports: [BrowserModule, HttpClientModule, FormsModule, AppRoutingModule],
      providers: [],
    });
    service = TestBed.inject(ProductService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
