import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'angularProject';
  productView = true;
  addProductView = false;

  showProduct() {
    this.productView = true;
    this.addProductView = false;
  }

  showAddProduct() {
    this.productView = false;
    this.addProductView = true;
  }
}
