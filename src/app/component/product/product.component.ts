import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/model/product';
import { ProductService } from 'src/app/service/product.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css'],
})
export class ProductComponent implements OnInit {
  products: Product[] = [];

  constructor(private productService: ProductService) {
    productService.getProducts().then(
      (products) => {
        this.products = products;
      },
      (error) => {
        console.error('products not found error: ' + error);
      }
    );
  }

  ngOnInit(): void {}
}
