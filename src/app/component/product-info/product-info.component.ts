import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from 'src/app/service/product.service';

@Component({
  selector: 'app-product-info',
  templateUrl: './product-info.component.html',
  styleUrls: ['./product-info.component.css'],
})
export class ProductInfoComponent implements OnInit {
  id: number = 0;
  product: any;

  constructor(private service: ProductService, private route: ActivatedRoute) {
    this.route.params.subscribe((params) => {
      this.id = params['id'];
      this.service.getProduct(this.id).then(
        (product) => {
          this.product = product;
        },
        (error) => {
          console.error('product not found error:' + error);
        }
      );
    });
  }

  ngOnInit(): void {}
}
