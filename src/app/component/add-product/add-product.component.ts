import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/model/product';
import { ProductService } from 'src/app/service/product.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css'],
})
export class AddProductComponent implements OnInit {
  constructor(private productService: ProductService) {}

  addProduct(product: Product) {
    console.log('addProduct() run');
    this.productService.createProduct(product);
  }

  ngOnInit(): void {}
}
